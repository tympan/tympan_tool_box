# tympan_tool_box

Python tool box of Code_TYMPAN.

Tutorials and user manual have been uploaded 'as is'.<br/>
Tutorial 0 has been fixed except export_spectre_src which seems to be broken even in Python script : cf issue #1

TODO :
- Organize python scripts under Code_TYMPAN/bin, as some scripts are used both for gui and tool box usages
- Test and update jupyter notebooks
- Update python scripts if necessary
