@echo off

set MY_TY_PATH=C:\Code_TYMPAN
set CONDA_ENV_PATH=C:\outils\miniconda3_64\envs\tybl64vs2017

echo "Configuring environment to run Code_TYMPAN GUI and Python scripts"

set PATH=%MY_TY_PATH%;%PATH% 
set TYMPAN_SOLVERDIR=%MY_TY_PATH%\plugins
set PYTHONPATH=%MY_TY_PATH%\cython;%MY_TY_PATH%\bin;%PYTHONPATH%
set PYTHONIOENCODING=UTF8
set CGAL_BINDINGS_PATH=%MY_TY_PATH%\cython\CGAL

set TYMPAN_PYTHON_INTERP=%CONDA_ENV_PATH%\python.exe

echo "My local path to Code_Tympan is set to %MY_TY_PATH%"